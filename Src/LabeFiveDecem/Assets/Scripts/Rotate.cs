using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Rotate : MonoBehaviour, IStrategy
{
    private float _speed = 10f;

    private Vector2 _rot;
    public void Perform(Transform trans)
    {
        trans.RotateAround(trans.transform.position, Vector2.down, _speed * Time.deltaTime);
    }
}
