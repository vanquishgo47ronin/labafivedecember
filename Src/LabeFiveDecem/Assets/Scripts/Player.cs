using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Performer _performer;

    private IStrategy _strategy;

    public Player(IStrategy strateg)
    {
        _strategy = strateg;
    }

    public Player(Performer perform)
    {
        _performer = perform;
        //perform = new Performer(new Rotate());
    }
}
